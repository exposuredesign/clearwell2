<?php
/*
Template Name: Services
*/

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main services" role="main">

			<!-- .title strip-->
			<div class="title-strip">
				<img src="<?php echo get_template_directory_uri();?>/images/services-title-bg.jpg">
				<div class="title-strip-text">
					<h1><?php echo get_the_title();?></h1>
				</div><!-- .title-strip-text -->
			</div><!-- .title-strip -->
			<div class="services-grid">
				<header>
					<?php
			while ( have_posts() ) : the_post();

				the_content();

			endwhile; // End of the loop.
			?>

				</header>
				
				<!-- .service cards grid-->
				<div class="container strip-services">
					<?php
			$services = get_post_meta($post->ID, 'services_services_group', true);
			foreach ($services as $item)
			{
			
				$img = wp_get_attachment_metadata($item['service_image'][0]);
				$upload_dir = wp_upload_dir();
			?>

			<div class="strip-services-item col-md-4 col-sm-6">
				<img src="<?php echo $upload_dir['baseurl'] .'/'.$img['file'];?>">
				<div class="strip-services-item-text">
					<h3><?php echo $item['service_title'];?></h3>
					<p><?php echo $item['service_text'];?></p>
					
				</div>
			</div> <!-- .strip-services-item -->
			<?php
			}
			?>

					
				</div>	<!-- .strip-sevices -->
			</div><!-- .services-grid -->	
		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
