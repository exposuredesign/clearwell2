<?php
/*
Template Name: Gallery
*/
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main gallery" role="main">

<?php
while ( have_posts() ) : the_post();
?>

<?php
endwhile; // End of the loop.
?>
<h1>Gallery</h1>
<div class="grid-filters filter-button-group">
	<button class="active" data-filter="*">show all</button>
  	<button data-filter=".services">services</button>
  	<button data-filter=".news">news</button>
  	<button data-filter=".hedge">hedge trimming</button>
</div>
			
			<div class="gallery-grid container">
			  <div class="grid">
			    <!-- 2 col grid @ xs, 3 col grid @ sm, 4 col grid @ md -->
			    <div class="grid-sizer col-xs-6 col-sm-4 col-md-4"></div>
			    <!-- 1 col @ xs, 2 col @ sm, 2 col @ md -->
			<?php
			$gallery = get_post_meta($post->ID, 'gallery_group', true);
			foreach ($gallery as $item)
			{
			
				$img = wp_get_attachment_metadata($item['gallery_image'][0]);
				$upload_dir = wp_upload_dir();
			?>	
			    <div class="grid-item col-xs-6 col-sm-4 col-md-4 <?php echo $item['gallery_category'];?>">
			      <div class="grid-item-content hovereffect">
			      	<img src="<?php echo $upload_dir['baseurl'] .'/'.$img['file']; ?>">
			      	 <div class="overlay">
			           <a class="info" href="<?php echo $upload_dir['baseurl'] .'/'.$img['file']; ?>" data-featherlight="image">
			           		<span class="glyphicon glyphicon-search"></span>
			           </a>
			        </div><!-- .overlay -->
			      </div>
			    </div><!-- .grid-item -->
			<?php
			} //end foreach
			?> 
			    
			    
			  </div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
