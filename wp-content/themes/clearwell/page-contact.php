<?php
/*
Template Name: Contact
*/
get_header(); ?>


	<div id="primary" class="content-area">
		<main id="main" class="site-main contact" role="main">

<?php
while ( have_posts() ) : the_post();
?>
			<div class="contact-title-strip">
				<div class="container">
					<h1><?php the_title();?></h1>
				</div>
			</div>

			<div class="contact-map container">
				<div class="contact-map-overlay" onClick="style.pointerEvents='none'"></div>
				<iframe
				  width="100%"
				  height="400"
				  frameborder="0" style="border:0"
				  src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDoBWCiIh2SPlA0k-C0iiloKvAsdkorkBM
				    &q=HX6+1NX" allowfullscreen>
				</iframe>
			</div>	

			<div class="contact-info container">
				<div class="contact-address col-sm-6">
					<h3>Contact</h3>
					<?php the_content(); ?>
				</div><!-- .contact-address -->
				<div class="contact-form col-sm-6">
				<?php echo do_shortcode('[contact-form-7 id="33" title="Contact Form"]'); ?>
				</div><!-- .contact-form -->
			</div><!-- .contact-info -->
<?php
endwhile; // End of the loop.
?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
