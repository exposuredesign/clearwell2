<?php
/*
Template Name: Home
*/
get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div id="carousel-example-generic" class="carousel slide">
   
	      <!-- Wrapper for slides -->
	      <div class="carousel-inner" role="listbox">
	      <?php
	      	$home_slides = get_post_meta($post->ID, 'home_slides_group', true);
            $i = 0;
			foreach ($home_slides as $slide)
			{
	      ?>

	        <!-- First slide -->
	        <div class="item <?php if ($i==0) { echo 'active';}?>">
	        	
	        	<?php echo wp_get_attachment_image($slide['slide_image'][0], 'full'); ?>
	          <div class="carousel-caption">
	            <h3 data-animation="animated bounceInUp">
	             <?php echo $slide['slide_title'];?>
	            </h3>
	            <hr data-animation="animated bounceInUp">
	            <h5 data-animation="animated bounceInUp">
	              <?php echo $slide['slide_subtitle'];?>
	            </h3>
	            <button class="btn btn-primary btn-lg" data-animation="animated zoomInUp" href="<?php echo $slide['slide_button_url'];?>"><?php echo $slide['slide_button_text'];?></button>
	          </div>
	        </div> <!-- /.item -->
	        <?php
		    $i++;
		    } //end for loop
		    ?>
	       

	      </div><!-- /.carousel-inner -->

	      <!-- Controls -->
	      <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
	        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
	        <span class="sr-only">Previous</span>
	      </a>
	      <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
	        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
	        <span class="sr-only">Next</span>
	      </a>
	    </div><!-- /.carousel -->
			
	    <div class="sales-pitch container">
	    	<div class="row is-flex">
	    	<div class="sales-pitch-col col-md-4 col-sm-12">
	    		<h2><?php echo get_post_meta($post->ID, 'home_sales_title', true);?></h2>
	    		<p class="sales-pitch-left"><?php echo get_post_meta($post->ID, 'home_sales_text', true);?></p>
	    	</div>
	    	<div class="sales-pitch-col col-md-4 col-sm-6 col-xs-12">
	    		<div class="sales-pitch-col-inner">
	    			<div>
	    			<?php $group =  get_post_meta($post->ID, 'home_sales_group1', true);?>
	    			<span class="glyphicon <?php echo $group['sales_icon'];?>"></span>
	    			<h3><?php echo $group['sales_title'];?></h3>
	    			<p><?php echo $group['sales_text'];?></p>
	    			</div>
	    			<div>
	    			<?php $group =  get_post_meta($post->ID, 'home_sales_group2', true);?>
	    			<span class="glyphicon <?php echo $group['sales_icon'];?>"></span>
	    			<h3><?php echo $group['sales_title'];?></h3>
	    			<p><?php echo $group['sales_text'];?></p>
	    			</div>
	    		</div>
	    	</div>
	    	<div class="sales-pitch-col col-md-4 col-sm-6 col-xs-12">
	    		<div class="sales-pitch-col-inner">
	    			<div>
	    			<?php $group =  get_post_meta($post->ID, 'home_sales_group3', true);?>
	    			<span class="glyphicon <?php echo $group['sales_icon'];?>"></span>
	    			<h3><?php echo $group['sales_title'];?></h3>
	    			<p><?php echo $group['sales_text'];?></p>
	    			</div>
	    			<div>
	    			<?php $group =  get_post_meta($post->ID, 'home_sales_group4', true);?>
	    			<span class="glyphicon <?php echo $group['sales_icon'];?>"></span>
	    			<h3><?php echo $group['sales_title'];?></h3>
	    			<p><?php echo $group['sales_text'];?></p>
	    			</div>
	    		</div>
	    	</div>
	    	</div><!-- .row is flex-->
	    </div><!-- .sales-pitch -->
		
		<div class="strip-contact clearfix">
			<div class="container">
				<div class="col-sm-10">
					<h2> Commercial tree care and grounds maintenance in Calderdale</h2>
				</div>
				<div class="col-sm-2 strip-contact-button">
					<button class="btn btn-primary btn-lg">Get in touch</button> 
				</div>
			</div> <!-- .container -->	
		</div><!-- .strip-contact -->

		<div class="container strip-services">
			<?php
			$services = get_post_meta($post->ID, 'home_services_group', true);
			foreach ($services as $item)
			{
			
				$img = wp_get_attachment_metadata($item['service_image'][0]);
				$upload_dir = wp_upload_dir();
			?>

			<div class="strip-services-item col-md-4 col-sm-6">
				<img src="<?php echo $upload_dir['baseurl'] .'/'.$img['file'];?>">
				<div class="strip-services-item-text">
					<h3><?php echo $item['service_title'];?></h3>
					<p><?php echo $item['service_text'];?></p>
					<?php if ($item['service_url'] !== '') {?>
					<a href="<?php echo $item['service_url'];?>">Read More</a>
					<?php } ?>
				</div>
			</div> <!-- .strip-services-item -->
			<?php
			}
			?>

		</div>	<!-- .strip-sevices -->
		<div class="testimonial">
			<div class="testimonial-text">
				<h1>John Smith</h1>
				<h2>Manager</h2>
				<p>
				Efficient, polite and speedy.  Major tree work done really well and everything left incredibly tidy! Would recommend to anyone and will definitely use again. 10/10
				</p>

			</div><!--.testimonial-text-->
		</div><!--.testimonial-->
		<div class="strip-logos ">
			<?php kw_sc_logo_carousel('homepage');?>
		</div><!-- .strip-logos -->
	</main><!-- #main -->
</div><!-- #primary -->

<?php

get_footer();
