<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Clearwell
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>
<?php
$theme_options = get_option('my_theme_settings');
?>
<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'clearwell' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<div class="header-top-light"></div>
		<div class="site-branding container">
			<div class="logo">
				<a href="<?php echo esc_attr(site_url()); ?>">
					<img src="<?php echo get_template_directory_uri();?>/images/clearwell-logo-300x79.png" alt="Clearwell Tree Services Logo">
				</a>	
			</div><!-- .logo -->
			<div class="header-info">
	          <ul>
	            <li> 
	              <span class="header-info-title">
	                <strong>
	                  E-mail:
	                </strong>
	              </span> 
	              <span>
	                <?php echo $theme_options['header_email'];?>
	              </span> 
	            </li>
	            <li> 
	              <span class="header-info-title">
	                <strong>
	                  Telephone:
	                </strong>
	              </span> 
	              <span>
	              	<?php echo $theme_options['header_telephone'];?>
	              </span> 
	            </li>
	          </ul>
			</div><!-- .header-info -->
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation container navbar-default" role="navigation">
			
			
		<div class="clearfix">
			<button type="button" class="navbar-toggle collapsed menu-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar top-bar"></span>
                        <span class="icon-bar middle-bar"></span>
                        <span class="icon-bar bottom-bar"></span>
            </button>
</div>
			<?php wp_nav_menu( array( 'theme_location' => 'menu-1', 'menu_id' => 'primary-menu' ) ); ?>
		</nav><!-- #site-navigation -->

	</header><!-- #masthead -->

	<div id="content" class="site-content">
