<?php
/*
 Title: Header Settings
 Setting: my_theme_settings
 */
?>
 <div class="piklist-demo-highlight">
  <?php _e('Header Logo and Footer Logo are currently hard coded.  Also the gallery widget reflects images from the gallery page.', 'piklist-demo');?>
</div>
<?php

piklist('field', array(
  'type' => 'text'
  ,'field' => 'header_email'
  ,'label' => 'Email Address'
  ,'description' => 'At top of page'
  ,'attributes' => array(
  'class' => 'regular-text',
  
  )
 ));

piklist('field', array(
  'type' => 'text'
  ,'field' => 'header_telephone'
  ,'label' => 'Telephone Number'
  ,'description' => 'At top of page'
  ,'attributes' => array(
  'class' => 'regular-text',
  
  )
 ));
 
