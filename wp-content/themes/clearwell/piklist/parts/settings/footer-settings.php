<?php
/*
 Title: Footer Settings
 Order: 2
 Setting: my_theme_settings
 */

piklist('field', array(
    'type' => 'textarea'
    ,'field' => 'footer_about'
    ,'label' => __('Footer About Text', 'piklist-demo')
    ,'attributes' => array(
      'rows' => 4
      ,'cols' => 50
      ,'class' => 'regular-text'
    )
  ));

piklist('field', array(
  'type' => 'text'
  ,'field' => 'footer1_title'
  ,'label' => 'Footer 1 Title'
  ,'attributes' => array(
  'class' => 'regular-text',
  
  )
 ));

piklist('field', array(
    'type' => 'textarea'
    ,'field' => 'footer1_text'
    ,'label' => __('Footer 1 Text', 'piklist-demo')
    ,'attributes' => array(
      'rows' => 4
      ,'cols' => 50
      ,'class' => 'regular-text'
    )
  ));

piklist('field', array(
  'type' => 'text'
  ,'field' => 'footer2_title'
  ,'label' => 'Footer 2 Title'
  ,'attributes' => array(
  'class' => 'regular-text',
  
  )
 ));

piklist('field', array(
    'type' => 'textarea'
    ,'field' => 'footer2_text'
    ,'label' => __('Footer 2 Text', 'piklist-demo')
    ,'attributes' => array(
      'rows' => 4
      ,'cols' => 50
      ,'class' => 'regular-text'
    )
  ));

piklist('field', array(
  'type' => 'text'
  ,'field' => 'footer3_title'
  ,'label' => 'Footer 3 Title'
  ,'attributes' => array(
  'class' => 'regular-text',
  
  )
 ));
 
