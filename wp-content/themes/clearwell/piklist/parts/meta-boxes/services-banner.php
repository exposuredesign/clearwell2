<?php
/*
Title: Header Banner
Post Type: page
Template: page-services
Order: 1
*/

piklist('field', array(
    'type' => 'file'
    ,'field' => 'services_banner'
    ,'scope' => 'post_meta'
    ,'label' => __('Header Banner','piklist')
    ,'description' => __('Header bg image for page title','piklist')
    ,'required' => true
    ,'options' => array(
      'modal_title' => __('Add Banner','piklist')
      ,'button' => __('Add','piklist')
    )
    ,'validate' => array(
      array(
        'type' => 'limit'
        ,'options' => array(
          'min' => 1
          ,'max' => 1
        )
      )
    )
  ));
