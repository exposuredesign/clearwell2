<?php
/*
Title: Home Carousel
Post Type: page
Template: page-home
Order: 1
*/

piklist('field', array(
    'type' => 'group'
    ,'field' => 'home_slides_group'
    ,'add_more' => true
    ,'label' => __('Home Slides', 'piklist-demo')
    ,'description' => __('Add slides to homepage slider.', 'piklist-demo')
    ,'fields' => array(

      array(
        'type' => 'file'
        ,'field' => 'slide_image'
        ,'scope' => 'post_meta'
        ,'label' => __('Slide Image', 'piklist-demo')
        ,'options' => array(
            'modal_title' => __('Add Thumbnail(s)', 'piklist-demo')
            ,'button' => __('Add', 'piklist-demo')
        )
       // ,'required' => true
      )

      ,array(
        'type' => 'textarea'
        ,'field' => 'slide_title'
        ,'label' => __('Slide Title', 'piklist-demo')
        ,'columns' => 12
        ,'attributes' => array(
	      'rows' => 1
	      ,'cols' => 30
	      ,'class' => 'large-text code'
	    )
      )
      ,array(
        'type' => 'textarea'
        ,'field' => 'slide_subtitle'
        ,'label' => __('Slide Subtitle', 'piklist-demo')
        ,'columns' => 12
        ,'attributes' => array(
	      'rows' => 2
	      ,'cols' => 30
	      ,'class' => 'large-text code'
	    )
      )

      ,array(
        'type' => 'text'
        ,'field' => 'slide_button_text'
        ,'label' => __('Button Text', 'piklist-demo')
        ,'columns' => 12
      )

      ,array(
        'type' => 'text'
        ,'field' => 'slide_button_url'
        ,'label' => __('Button URL', 'piklist-demo')
        ,'columns' => 12
      )
      
      


    )
    ,'on_post_status' => array(
        'value' => 'lock'
      )
  ));