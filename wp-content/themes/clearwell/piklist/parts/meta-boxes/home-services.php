<?php
/*
Title: Home Services
Post Type: page
Template: page-home
Order: 3
*/

piklist('field', array(
    'type' => 'group'
    ,'field' => 'home_services_group'
    ,'add_more' => true
    ,'label' => __('Home Services', 'piklist-demo')
    ,'fields' => array(

      array(
        'type' => 'file'
        ,'field' => 'service_image'
        ,'scope' => 'post_meta'
        ,'label' => __('Service Image', 'piklist-demo')
        ,'options' => array(
            'modal_title' => __('Add Thumbnail(s)', 'piklist-demo')
            ,'button' => __('Add', 'piklist-demo')
        )
       // ,'required' => true
      )

      ,array(
        'type' => 'text'
        ,'field' => 'service_title'
        ,'label' => __('Service Title', 'piklist-demo')
        ,'columns' => 12
        ,'attributes' => array(
	      'rows' => 1
	      ,'cols' => 30
	      ,'class' => 'large-text code'
	    )
      )
      ,array(
        'type' => 'textarea'
        ,'field' => 'service_text'
        ,'label' => __('Service Subtitle', 'piklist-demo')
        ,'columns' => 12
        ,'attributes' => array(
	      'rows' => 3
	      ,'cols' => 30
	      ,'class' => 'large-text code'
	    )
      )


      ,array(
        'type' => 'text'
        ,'field' => 'service_url'
        ,'label' => __('Service URL', 'piklist-demo')
        ,'columns' => 12
      )
      
      


    )
    ,'on_post_status' => array(
        'value' => 'lock'
      )
  ));