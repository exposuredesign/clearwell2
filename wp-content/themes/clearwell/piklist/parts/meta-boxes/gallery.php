<?php
/*
Title: Gallery
Post Type: page
Template: page-gallery
Order: 1
*/

piklist('field', array(
    'type' => 'group'
    ,'field' => 'gallery_group'
    ,'add_more' => true
    ,'label' => __('Gallery Images', 'piklist-demo')
    ,'description' => __('Add images to the gallery.', 'piklist-demo')
    ,'fields' => array(

      array(
    'type' => 'select',
    'field' => 'gallery_category',
    'label' => 'Category',
    'choices' => array(
      'services' => 'Services',
      'news' => 'News',
      'hedge trimming' => 'Hedge Trimming'
       )
    )

      ,array(
        'type' => 'file'
        ,'field' => 'gallery_image'
        ,'scope' => 'post_meta'
        ,'label' => __('Slide Image', 'piklist-demo')
        ,'options' => array(
            'modal_title' => __('Add Thumbnail(s)', 'piklist-demo')
            ,'button' => __('Add', 'piklist-demo')
        )
       // ,'required' => true
      )

      

      ,array(
        'type' => 'text'
        ,'field' => 'gallery_caption'
        ,'label' => __('Caption', 'piklist-demo')
        ,'columns' => 12
      )

      
      


    )
    ,'on_post_status' => array(
        'value' => 'lock'
      )
  ));