<?php
/*
Title: Services Items
Post Type: page
Template: page-services
Order: 2
*/

piklist('field', array(
    'type' => 'group'
    ,'field' => 'services_services_group'
    ,'add_more' => true
    ,'label' => __('Services Items', 'piklist-demo')
    ,'fields' => array(

      array(
        'type' => 'file'
        ,'field' => 'service_image'
        ,'scope' => 'post_meta'
        ,'label' => __('Service Image', 'piklist-demo')
        ,'options' => array(
            'modal_title' => __('Add Thumbnail(s)', 'piklist-demo')
            ,'button' => __('Add', 'piklist-demo')
        )
       // ,'required' => true
      )

      ,array(
        'type' => 'text'
        ,'field' => 'service_title'
        ,'label' => __('Service Title', 'piklist-demo')
        ,'columns' => 12
        ,'attributes' => array(
	      'rows' => 1
	      ,'cols' => 30
	      ,'class' => 'large-text code'
	    )
      )
      ,array(
        'type' => 'textarea'
        ,'field' => 'service_text'
        ,'label' => __('Service Subtitle', 'piklist-demo')
        ,'columns' => 12
        ,'attributes' => array(
	      'rows' => 3
	      ,'cols' => 30
	      ,'class' => 'large-text code'
	    )
      )


      
      
      


    )
    ,'on_post_status' => array(
        'value' => 'lock'
      )
  ));