<?php
/**
 * Clearwell functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Clearwell
 */

if ( ! function_exists( 'clearwell_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function clearwell_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Clearwell, use a find and replace
	 * to change 'clearwell' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'clearwell', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'clearwell' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'clearwell_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'clearwell_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function clearwell_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'clearwell_content_width', 640 );
}
add_action( 'after_setup_theme', 'clearwell_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function clearwell_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'clearwell' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'clearwell' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'clearwell_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function clearwell_scripts() {
	//bootstrap css
	wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/bootstrap/css/bootstrap.min.css' );
	//googe fonts - chivo
	wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Chivo:300,400,400i,700', false );
	//animate.css
	wp_enqueue_style( 'animate-css', get_template_directory_uri() . '/css/animate.css' );
	//featherlight css
	wp_enqueue_style( 'featherlight-css', get_template_directory_uri() . '/css/featherlight.min.css' );
	//mainstyle sheet
	wp_enqueue_style( 'clearwell-style', get_stylesheet_uri(), false, filemtime(get_template_directory() . '/style.css') );
	//bootstrap js
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/bootstrap/js/bootstrap.min.js', array('jquery'), '3.3.7', true );
	wp_enqueue_script( 'clearwell-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'clearwell-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	wp_enqueue_script( 'animate-carousel', get_template_directory_uri() . '/js/animate-carousel.js', array(), '20151215', true );
	wp_enqueue_script( 'masonry', get_template_directory_uri() . '/js/masonry.min.js', array(), '20151215', true );
	wp_enqueue_script( 'isotope', get_template_directory_uri() . '/js/isotope.min.js', array(), '20151215', true );
	wp_enqueue_script( 'imagesloaded', get_template_directory_uri() . '/js/imagesloaded.min.js', array(), '20151215', true );

	wp_enqueue_script( 'featherlight-js', get_template_directory_uri() . '/js/featherlight.min.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'clearwell_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/***
 * Register secondary menus.
 */
function register_my_menus() {
  register_nav_menus(
    array(
      'footer-menu' => __( 'Footer Menu' )
    )
  );
}
add_action( 'init', 'register_my_menus' );

/*PIKLIST STUFF*/
//remove page custom fields so piklist works in pages
add_action( 'init', 'my_custom_init' );
function my_custom_init() {
    remove_post_type_support( 'page', 'custom-fields' );
    remove_post_type_support( 'post', 'custom-fields' );
}

//remove page default editors so you can add piklist ones
add_action('init', 'remove_unused_editors');

function remove_unused_editors() {
    $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
    $template_file = get_post_meta($post_id,'_wp_page_template',TRUE);

    switch ($template_file) {
        //case 'page-credits.php': - we are using this one
        case 'page-home.php':
            remove_post_type_support('page', 'editor');
            remove_post_type_support('page', 'thumbnail');
        case 'page-services.php':
            remove_post_type_support('page', 'thumbnail');
        case 'page-gallery.php':
            remove_post_type_support('page', 'editor');
            remove_post_type_support('page', 'thumbnail');        
    }
}

//add theme settings page for header and footer content
add_filter('piklist_admin_pages', 'piklist_theme_setting_pages');
  function piklist_theme_setting_pages($pages)
  {
     $pages[] = array(
      'page_title' => __('Custom Settings')
      ,'menu_title' => __('Settings', 'piklist')
      ,'sub_menu' => 'themes.php' //Under Appearance menu
      ,'capability' => 'manage_options'
      ,'menu_slug' => 'custom_settings'
      ,'setting' => 'my_theme_settings'
      ,'menu_icon' => plugins_url('piklist/parts/img/piklist-icon.png')
      ,'page_icon' => plugins_url('piklist/parts/img/piklist-page-icon-32.png')
      ,'single_line' => true
      ,'default_tab' => 'Basic'
      ,'save_text' => 'Save Theme Settings'
    );
 
    return $pages;
  }

  // Removes from admin menu
add_action( 'admin_menu', 'my_remove_admin_menus' );
function my_remove_admin_menus() {
    remove_menu_page( 'edit-comments.php' );
}
// Removes from post and pages
add_action('init', 'remove_comment_support', 100);

function remove_comment_support() {
    remove_post_type_support( 'post', 'comments' );
    remove_post_type_support( 'page', 'comments' );
}
// Removes from admin bar
function mytheme_admin_bar_render() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('comments');
}
add_action( 'wp_before_admin_bar_render', 'mytheme_admin_bar_render' );

//remove standard post type
add_action('admin_menu','remove_default_post_type');

function remove_default_post_type() {
	remove_menu_page('edit.php');
}
