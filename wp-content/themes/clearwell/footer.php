<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Clearwell
 */

?>

	</div><!-- #content -->
<?php
$theme_options = get_option('my_theme_settings');
?>
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="container">
			<div class="row">
				<div class="footer-col about col-md-3 col-sm-12">
					<img src="<?php echo get_template_directory_uri();?>/images/clearwell-logo.png" width="230px">
					<p>
					<?php echo $theme_options['footer_about'];?>
					</p>
				</div>
				<div class="footer-col services col-md-3 col-sm-4">
				<h3><?php echo $theme_options['footer1_title'];?></h3>
				<?php echo $theme_options['footer1_text'];?>

				
				</div>
				<div class="footer-col col-md-3 col-sm-4">
				<h3><?php echo $theme_options['footer2_title'];?></h3>
				<?php echo $theme_options['footer2_text'];?>
				</div>
				<div class="footer-col col-md-3 col-sm-4">
				
				<img src="<?php echo get_template_directory_uri();?>/images/footer-logos.png">
				</div>
			</div>
		</div>	

		<div class="footer-strip">
		<div class="site-info clearfix container">
			<div class="copyright">
			<p>© Copyright 2017 Clearwell Tree Services Ltd.</p>
			</div>

			<div >
			<?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
			</div>
			
		</div><!-- .site-info -->
		</div><!-- .footer-strip -->
	</footer><!-- #colophon -->
</div><!-- #page -->
<script type="text/javascript">
jQuery(document).ready(function($) {
    
    $('.gallery-grid').imagesLoaded( function() {
  		$('.grid').isotope({
	  		itemSelector: '.grid-item', // use a separate class for itemSelector, other than .col-
	  		columnWidth: '.grid-sizer',
	  		percentPosition: true
		});
	});


    

	//filter buttons
	$('.filter-button-group').on( 'click', 'button', function() {
  		$(this).addClass('active').siblings().removeClass('active');
  		var filterValue = $(this).attr('data-filter');
  		$('.grid').isotope({ filter: filterValue });
	});
});

	
</script>
<?php wp_footer(); ?>

</body>
</html>
